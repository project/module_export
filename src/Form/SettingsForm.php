<?php

namespace Drupal\module_export\Form;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Archiver\ArchiveTar;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\FileRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class Module Export Settings Form.
 *
 * @package Drupal\module_export\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The module extension list service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected ModuleExtensionList $moduleExtensionList;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The file repository service.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected FileRepositoryInterface $fileRepository;

  /**
   * Constructs a module export settings form.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   *   The module extension list service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleExtensionList $module_extension_list, FileSystemInterface $file_system, FileRepositoryInterface $file_repository) {
    parent::__construct($config_factory);
    $this->moduleExtensionList = $module_extension_list;
    $this->fileSystem = $file_system;
    $this->fileRepository = $file_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('extension.list.module'),
      $container->get('file_system'),
      $container->get('file.repository'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'module_export_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['name'] = [
      '#title' => $this->t('Exported Module Name'),
      '#description' => $this->t('Choose a module name Example: Studio Machine @suggestion', ['@suggestion' => '(Do not begin name with numbers.)']),
      '#type' => 'textfield',
      '#attributes' => ['class' => ['profile-name']],
      '#field_suffix' => '<span class="field-suffix"></span>',
      '#required' => TRUE,
    ];

    $form['description'] = [
      '#title' => $this->t('Description'),
      '#description' => $this->t('Provide a short description for the module.'),
      '#type' => 'textfield',
      '#required' => TRUE,
    ];

    $form['module_type'] = [
      '#title' => $this->t('Select Module Types'),
      '#description' => $this->t('Choose which type of modules to be selected to export.'),
      '#type' => 'select',
      '#options' => [
        -1 => $this->t('All'),
        1 => $this->t('Enabled'),
        0 => $this->t('Disabled'),
      ],
      '#default_value' => 1,
    ];

    $form['format'] = [
      '#title' => $this->t('Export format'),
      '#type' => 'radios',
      '#options' => [
        0 => $this->t('Module'),
        1 => $this->t('CSV'),
      ],
      '#default_value' => 0,
    ];

    $form['check_version'] = [
      '#title' => $this->t('Check Minor Version'),
      '#description' => $this->t('If it checked, the all installable module version should be the same.'),
      '#type' => 'checkbox',
      '#default_value' => 0,
      '#states' => [
        'visible' => [
          ':input[name="format"]' => ['value' => 0],
        ],
      ],
    ];

    $form['use_module'] = [
      '#type' => 'markup',
      '#markup' => '<p><h4>How to use the exported module.</h4></p><p>Download and extract the module into the modules folder of another Drupal installation. You can then review the module dependencies to check the status of the module or enable it to enable all the required modules.</p>',
      '#states' => [
        'visible' => [
          ':input[name="format"]' => ['value' => 0],
        ],
      ],
    ];

    $form['download_module'] = [
      '#type' => 'submit',
      '#value' => $this->t('Download Module'),
      '#attributes' => [
        'class' => [
          'button--primary',
        ],
      ],
      '#states' => [
        'visible' => [
          ':input[name="format"]' => ['value' => 0],
        ],
      ],
      '#submit' => [
        [$this, 'moduleExportdownloadModule'],
      ],
    ];

    $form['download_csv'] = [
      '#type' => 'submit',
      '#value' => $this->t('Download CSV'),
      '#attributes' => [
        'class' => [
          'button--primary',
        ],
      ],
      '#states' => [
        'visible' => [
          ':input[name="format"]' => ['value' => 1],
        ],
      ],
      '#submit' => [
        [$this, 'moduleExportdownloadcsv'],
      ],
    ];

    $settings_form = parent::buildForm($form, $form_state);
    unset($settings_form['actions']['submit']);
    return $settings_form;
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['module_export.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $module_name = $form_state->getValue(['name']);
    // Validate text field to only contain numeric values.
    if (preg_match('/^\d/', $module_name) === 1) {
      $form_state->setErrorByName('name', $this->t('Module name should not start with a number.'));
    }
  }

  /**
   * Submit handler for download module as tar file.
   */
  public function moduleExportdownloadModule($form, FormStateInterface $form_state) {
    // Save form submissions.
    $name = $form_state->getValue('name');
    $description = $form_state->getValue('description');
    $module_type = $form_state->getValue('module_type');
    $check_version = $form_state->getValue('check_version');

    $machine_name = preg_replace('/_+/', '_', preg_replace('/[^a-z0-9]/', '_', strtolower($name)));
    $filename = $machine_name . '.info.yml';
    $dir = 'public://' . $machine_name;
    // Create archive file.
    $this->createArchive($name, $description, $machine_name, $filename, $dir, $module_type, $check_version);

    $response = new BinaryFileResponse($dir . '/' . $machine_name . '.tar.gz');
    $filename = $machine_name . '.tar.gz';
    $response->setContentDisposition('attachment', $filename);
    $form_state->setResponse($response);
  }

  /**
   * Submit handler for download modules information as CSV.
   */
  public function moduleExportdownloadcsv($form, FormStateInterface $form_state) {
    // Save form submissions.
    $name = $form_state->getValue('name');
    $module_type = $form_state->getValue('module_type');
    $machine_name = preg_replace('/_+/', '_', preg_replace('/[^a-z0-9]/', '_', strtolower($name)));
    $dir = 'public://';
    $this->moduleExportGetcsvContent($module_type, $dir, $machine_name);
    $filename = $machine_name . '.csv';
    $response = new BinaryFileResponse($dir . '/' . $filename);
    $response->setContentDisposition('attachment', $filename);
    $form_state->setResponse($response);
  }

  /**
   * Get CSV file content.
   */
  public function moduleExportGetcsvContent($module_type, $dir, $machine_name) {
    // Get enabled modules list.
    $modules_array = $this->moduleExportGetModulesList($module_type);
    $get_available_updates = update_get_available(TRUE);
    $filename = $machine_name . '.csv';
    $output = "Title, Package, Installed Version, Current Version, Is latest, Module Url, Dependency, Status \n";

    foreach ($modules_array as $modules_info) {
      foreach ($modules_info as $module) {
        $parent_module = [];
        // Parse module info yml file.
        $module_path = $this->moduleExtensionList->getPath($module['filename']) . "/" . $module['filename'] . ".info.yml";
        $info_file = Yaml::decode(file_get_contents($module_path));
        if ($module['origin'] == 'core') {
          $parent_module = $get_available_updates['drupal'];
        }
        else {
          $parent_project = $info_file['project'] ?? $module['filename'];
          // Get parent module.
          $parent_module = $get_available_updates[$parent_project] ?? [];
        }

        // Fetch project dependencies.
        $dependencies = $info_file['dependencies'] ?? [];

        // Get project link.
        $url = $parent_module['link'] ?? '';

        // Get project releases.
        $releases = $parent_module['releases'] ?? [];

        // Check if current version is latest.
        $latest_release = '';
        if (is_array($releases)) {
          reset($releases);
          $latest_release = key($releases);

          if (isset($latest_release) && $latest_release == $module['version']) {
            $is_latest = $this->t('Yes');
          }
          elseif (isset($latest_release)) {
            $is_latest = $this->t('No');
          }
          else {
            $is_latest = '';
          }
        }

        // Replace comma from package name.
        $current_version = (isset($latest_release)) ? $latest_release : '';
        $module['status'] == 1 ? $status = 'Enabled' : $status = 'Disabled';
        $output .= "{$module['name']}, {$module['package']}, {$module['version']}, {$current_version}, {$is_latest}, {$url},";
        if (!empty($dependencies)) {
          $len = count($dependencies);
          foreach ($dependencies as $key => $dependency) {
            $module_dependency = str_replace('drupal:', '', $dependency);
            if ($key == $len - 1) {
              $output .= $module_dependency . ',';
            }
            else {
              $output .= $module_dependency . '|';
            }
          }
        }
        else {
          $output .= ' ,';
        }
        $output .= $status . "\n";
      }
    }

    return $this->fileRepository->writeData($output, $dir . '/' . $filename, FileSystemInterface::EXISTS_REPLACE);
  }

  /**
   * Get module file content.
   */
  public function moduleExportGetModulesList($module_type) {
    // Get all modules.
    $modules = $this->moduleExtensionList->reset()->getList();
    // Sort modules by name.
    uasort($modules, [ModuleExtensionList::class, 'sortByName']);
    $modules_list = [];
    foreach ($modules as $filename => $module) {
      $package = $module->info['package'];
      if ($module_type == -1 || $module_type == $module->status) {
        $modules_list[$package][] = [
          'filename' => $filename,
          'name' => $module->info['name'],
          'package' => $package,
          'version' => $module->info['version'],
          'status' => $module->status,
          'origin' => $module->origin ?? '',
        ];
      }
    }
    return $modules_list;
  }

  /**
   * Create a tarball for module.
   */
  public function createArchive($name, $description, $machine_name, $filename, $dir, $module_type, $check_version) {
    $this->fileSystem->delete($dir . '/' . $machine_name . '.tar.gz');
    // Create directory.
    $this->fileSystem->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY);
    // Get files.
    $files = [];
    $files[] = $this->moduleExportCreateInfoFile($name, $description, $dir, $machine_name, $module_type);
    $files[] = $this->moduleExportModuleFileContent($name, $machine_name, $dir, $module_type, $check_version);

    $tar_ob = new ArchiveTar($dir . '/' . $machine_name . '.tar.gz');
    $file_path = [];
    foreach ($files as $file) {
      $file_path[] = $file->getFileUri();
    }

    return $tar_ob->addModify($file_path, $machine_name, $dir);
  }

  /**
   * Get module file content.
   */
  public function moduleExportModuleFileContent($name, $machine_name, $dir, $module_type, $check_version) {
    $filename = $machine_name . '.module';
    $file_content = "<?php \n";
    $file_content .= "/** \n ";
    $file_content .= "* @file \n";
    $file_content .= " * Module file for {$name} \n";
    $file_content .= " */ \n\n";
    if ($check_version == 1) {
      $file_content .= $this->moduleExportGetModuleCode($machine_name, $module_type);
    }

    return $this->fileRepository->writeData($file_content, $dir . '/' . $filename, FileSystemInterface::EXISTS_REPLACE);
  }

  /**
   * Get module file content if check version is enabled.
   */
  public function moduleExportGetModuleCode($machine_name, $module_type) {
    // Get modules list.
    $modules_list = $this->moduleExportGetModulesList($module_type);
    $module_file = 'function ' . $machine_name . '_module_preinstall($module) {';
    $module_file .= "\n";
    $module_file .= "\t" . '$modules = [];' . "\n";
    foreach ($modules_list as $modules_array) {
      foreach ($modules_array as $module_info) {
        $module_machine_name = "'" . $module_info['filename'] . "'";
        $module_file .= "\t" . '$modules[' . $module_machine_name . '] = "' . $module_info['version'] . '";';
        $module_file .= "\n";
      }
    }
    // Get all modules.
    $module_file .= "\t" . '$site_modules = ' . "\Drupal::service('extension.list.module')->reset()->getList();" . "\n";
    $module_file .= "\t" . '$modules_list = [];' . "\n";
    $module_file .= "\t" . 'foreach ($site_modules as $filename => $module_detail) {' . "\n";
    $module_file .= "\t\t" . '$modules_list[$filename] = $module_detail->info["version"];' . "\n";
    $module_file .= "\t" . '}' . "\n";
    $module_file .= "\t" . 'foreach ($modules_list as $name => $module_info) {
      if (isset($modules[$name]) && $module_info != $modules[$name]) {
        \Drupal::messenger()->addError(t(\'@name version is not compatible.\', [
          \'@name\' => $name,
        ]));
        \Drupal::service(\'module_installer\')->uninstall([$name]);
      }
    }
    ';
    $module_file .= '}';

    return $module_file;
  }

  /**
   * Get info file content.
   */
  public function moduleExportCreateInfoFile($name, $description, $dir, $machine_name, $module_type) {
    $filename = $machine_name . '.info.yml';
    // Get list of active modules.
    $modules_list = $this->moduleExportGetModulesList($module_type);

    $info_file = "name: {$name}\n";
    if (!empty($description)) {
      $info_file .= "description: {$description}\n";
    }
    $info_file .= "core_version_requirement: ^9.3 || ^10\n";
    $info_file .= "type: module\n";

    $info_file .= "dependencies:\n";
    foreach ($modules_list as $package => $module_info) {
      $info_file .= "  # {$package}\n";
      foreach ($module_info as $module_name) {
        if ($module_name['origin'] == 'core') {
          $info_file .= "  - drupal:{$module_name['filename']}\n";
        }
        else {
          $info_file .= "  - {$module_name['filename']}:{$module_name['filename']}\n";
        }
      }
    }
    $info_file .= "\n";
    return $this->fileRepository->writeData($info_file, $dir . '/' . $filename, FileSystemInterface::EXISTS_REPLACE);
  }

}
